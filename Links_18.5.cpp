﻿
#include <iostream>
#include <string>
using namespace std;

class Player {
public:

        string name;
        int score;
    
    Player(string n, int s) {
        name = n;
        score = s;
    }
};
    void swap(Player& p1, Player& p2) {
        Player temp = p1;
        p1 = p2;
        p2 = temp;

    }
    void Bubble(Player arr[], int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j].score > arr[j + 1].score) {
                    swap(arr[j], arr[j + 1]);
                }
            }
    }

    
}

    int main()
    {
        Player people[] = { Player("Sigward:",256), Player("Laura:",821), Player("Ithan:", 696)};
        int n = sizeof(people) / sizeof(people[0]);
        Bubble(people, n);
        for (int i = 0; i < n; i++) 
        {
            cout << people[i].name << "" << people[i].score << endl;
        }

        return 0;
    }